<header>
    <div class="row" style="background-color:lavender">
        <div class="col-sm-4">
            <a href="/"> <img width="240" height="40" src="/images/logo1.png" style="margin:10px; margin-left:50px; margin-top:20px" /> </a>
        </div>

        <div class="col-sm-6" style="margin-top: 30px">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="#">Profile</a>
                </li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Messages ({{$parent.getMessageCount()}})<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li ng-repeat="message in $parent.messages">
                            <a href="/pages/sendMessage.php?tid={{ message.message_id }}">
                            <div style="margin:5px">
                                <img width="25" height="25" src="{{ message.sender_image_link }}" />
                                <b> {{ message.sender_name }}</b>
                                <br/>
                                <label>{{ message.subject }}</label>
                            </div> </a>
                            <hr/>
                        </li>
                    </ul>
                </li>
                
                <li>
                    <a href="about">About</a>
                </li>
                <li>
                    <a href="contact">Contact us</a>
                </li>
            </ul>
        </div>

        <div class="col-sm-2">
            <login-bar></login-bar>
        </div>
    </div>
    <div style="float:left">

</header>