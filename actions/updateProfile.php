<?php
include $_SERVER['DOCUMENT_ROOT'] . '/db/db_connect.php';

$account_type = getAccountType();
if ($account_type == 'student') {
    header("location: ../pages/updateStudentProfile.php");
} else if ($account_type == 'company') {
    header("location: ../pages/updateCompanyProfile.php");
} else {
    echo "Undefined type.";
}
?>
