startupHubApp.controller('MessageController', ['$scope', '$http',
function($scope, $http) {
    $scope.isSendingMessage = false;
    $scope.errorMessage = "";
    $scope.messages = [];
    
    $scope.log = function(message) {
        document.getElementById("divLog").innerHTML = angular.toJson(message, true);
    };
    
    $scope.threadInit = function(thread_id, to_id, related_post_id) {
        if (thread_id == "") {
            $http.post('/db/getMessageThreadByID.php', {'to_id': to_id, "post_id": related_post_id }).success(function(response) {
                $scope.log(response);
                if (response == "") {
                    //start new thread
                } else {
                    window.location.href = "/pages/sendMessage.php?tid=" + response.thread_id;                
                }
            }).error(function(data, status) {
                
            });
        } else {
            $http.post('/db/getMessageThreadByID.php', { "thread_id" : thread_id }).success(function(response) {
                $scope.log(response);
                $scope.messages = response.messages;
                $scope.message.subject = response.messages[0].subject;
                $scope.isSubjectDisabled = true;
                $scope.log(response);
            }).error(function(data,status) {
                alert(data+status);
            });
        }
    };
    
    $scope.sendMessage = function(message) {
        $scope.isSendingMessage = true;
        $http.post('/db/sendMessage.php', message).success(function(response) {
            $scope.log(message);
            if (response == "success") {
                alert("Message sent successfully.");
                location.reload();
                //window.location.href = "/index.php";
            } else {
                alert(response);
                $scope.isSendingMessage = false;
                $scope.errorMessage = response;
            }
        }).error(function(data, status) {
            $scope.isSendingMessage = false;
            $scope.errorMessage = status;
            alert(data + status);
        });
    };

}]);
