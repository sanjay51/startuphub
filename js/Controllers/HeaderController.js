startupHubApp.controller('HeaderController', ['$scope', '$http', function($scope, $http) {
    $scope.context = {
        title: 'StartupHubApp'
    };
    $scope.messages = [];
    
    $scope.getMessageCount = function() {
        return $scope.messages.length;
    };
        
    $scope.toReadable = function(str) {
        return angular.toJson(str);
    };
    
    $scope.sampleData = { "user" : "xyz" };
    $http.get('/db/getMessages.php', $scope.sampleData).success(function(response) {
        $scope.messages = response.messages;
        
        }).error(function(data, status, header, config) {
            alert($scope.toReadable(data) + $scope.toReadable(status) + $scope.toReadable(header) + $scope.toReadable(config));
        });
}]);
