startupHubApp.controller('PostCreationController', ['$scope', '$http',
function($scope, $http) {
    $scope.isCreatingPost = false;
    $scope.errorMessage = "";
    $scope.createPost = function(post) {
        $scope.isCreatingPost = true;
        $http.post('/db/createPost.php', post).success(function(response) {
            if (response == "success") {
                alert("Post created successfully.");
                window.location.href = "/";
            } else {
                $scope.isCreatingPost = false;
                $scope.errorMessage = response;
                alert(response);
            }
        }).error(function(data, status) {
            $scope.isCreatingPost = false;
            $scope.errorMessage = status;
            alert(data + status);
        });
    };
    
    $scope.log = function(message) {
        document.getElementById("divLog").innerHTML = angular.toJson(message, true);
    };

}]);
