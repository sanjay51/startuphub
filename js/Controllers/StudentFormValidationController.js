startupHubApp.controller('StudentFormValidationController', ['$scope', '$http',
function($scope, $http) {
    $http.get('../db/getUserProfile.php').success(function(response) {
        $scope.user = response;
        $scope.user.contact = Number(response.contact);
        $scope.user.present_year = Number(response.current_year);
        $scope.user.stream = response.branch;
        $scope.user.other_academic_details = response.academic_details;
    });

    $scope.update = function(user) {
        $http.post('../db/update_profile.php', user).success(function(response) {
            alert(response);
        }).error(function(data, status) {
            alert(data + status);
        });
    };

}]);
