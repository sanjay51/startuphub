startupHubApp.controller("PostsController", ['$scope', '$http',
function($scope, $http) {
    $http.get("db/getAllPosts.php").success(function(response) {
        $scope.posts = response.records;
    });
    
    $scope.addReplyToScope = function(keyEvent, content, replyText, userID, displayName, image) {
        $scope.log($scope.posts);
        if (keyEvent.which === 13) {
            angular.forEach ($scope.posts, function(post, index) {
                if (post.post_id == content.post_id) {
                    reply = { "reply_id":"199", "creator_name": displayName, "parent_id": userID, "creator_image_link": image, "reply_text": replyText };
                    $http.post("db/addPostReply.php", { "post_id" : content.post_id, "reply_text" : replyText })
                            .success( function (data, status, headers, config) {
                        if (data == "1") {
                            reply.bgColor = "white";
                        }
                    });
                    reply.bgColor = "red";
                    post.comments.push(reply);
                }
            });
        }
    };
    
    $scope.askForResume = function(toUserID, relatedPostID) {
        notification = { "to_user_id" : toUserID, "related_post_id": relatedPostID, "type" : '1'};
        $http.post("db/notifyUser.php", notification)
                .success (function (data, status, headers, config) {
                    alert("Thank you. The candidate has been notified. ");
                    $scope.log(data);
                });
        return true;
    };
    
    $scope.sendMessage = function(toUserID, relatedPostID, toUserName) {
        window.location.assign("/pages/sendMessage.php?to=" + toUserID + "&post=" + relatedPostID + "&name=" + toUserName);
    };
    
    $scope.createPost = function() {
        window.location.assign("/pages/createPost.php");
    };
    
    $scope.log = function(message) {
        document.getElementById("divLog").innerHTML = angular.toJson(message, true);
    };
}]);