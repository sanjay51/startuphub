startupHubApp.controller('ProfileController', ['$scope', '$http', function($scope, $http) {
    $scope.context = {
        title: 'StartupHubApp'
    };
    $scope.profile = {};
    
    user = {};
    $http.post('/db/getUserProfile.php', user).success(function(response) {
        $scope.profile = response.profile;
        
        }).error(function(data, status) {
            alert(data + status);
        });
}]);
