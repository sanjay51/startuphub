startupHubApp.controller('LoginController', ['$scope', '$http',
function($scope, $http) {
    $scope.isProcessingLogin = false;
    $scope.errorMessage = "";
    $scope.login = function(user) {
        $scope.isProcessingLogin = true;
        $http.post('../session/login-exec.php', user).success(function(response) {
            if (response == "success") {
                alert("Logged in successfully.");
                window.location.href = "/index.php";
            } else {
                alert(response);
                $scope.isProcessingLogin = false;
                $scope.errorMessage = response;
            }
        }).error(function(data, status) {
            $scope.isProcessingLogin = false;
            $scope.errorMessage = status;
            alert(data + status);
        });
    };

}]);
