startupHubApp.controller('RegistrationController', ['$scope', '$http',
function($scope, $http) {
    $scope.isProcessingRegistration = false;
    $scope.errorMessage = "";
    $scope.register = function(user) {
        $scope.isProcessingRegistration = true;
        $http.post('../session/register-exec.php', user).success(function(response) {
            if (response == "success") {
                alert("You have been registered successfully.");
                window.location.href = "/pages/login.php";
            } else {
                $scope.isProcessingRegistration = false;
                $scope.errorMessage = response;
            }
        }).error(function(data, status) {
            $scope.isProcessingRegistration = false;
            $scope.errorMessage = status;
            alert(data + status);
        });
    };

}]);
