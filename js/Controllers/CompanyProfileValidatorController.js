startupHubApp.controller('CompanyProfileValidationController', ['$scope', '$http',
function($scope, $http) {
    $scope.isProcessingUpdation = false;
    $scope.errorMessage = "";

    $http.get('../db/getCompanyProfile.php').success(function(response) {
        $scope.company = response;
        $scope.company.contact = Number(response.contact);
    });

    $scope.update = function(company) {
        $scope.isProcessingUpdation = true;
        $http.post('../db/updateCompanyProfile.php', company).success(function(response) {
            if (response == "success") {
                alert("Profile saved successfully.");
            } else {
                $scope.errorMessage = response;
            }

            $scope.isProcessingUpdation = false;
        }).error(function(data, status) {
            $scope.isProcessingUpdation = false;
            $scope.errorMessage = status;
            alert(data + status);
        });
    };

}]);
