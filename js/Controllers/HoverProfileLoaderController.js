startupHubApp.controller('HoverProfileLoaderController', ['$scope', '$http',
function($scope, $http) {
    $scope.data = "";
    $scope.isProfileLoaded = false;

    $scope.loadHoverProfile = function(user_id) {
        user = { "user_id" : user_id };
        $http.post("db/getHoverProfile.php", user).success(function(response) {
            $scope.data = response;
            $scope.isProfileLoaded = false;
        });
    };

    /*
     $scope.isProcessingLogin = false;
     $scope.errorMessage = "";
     $scope.login = function(user) {
     $scope.isProcessingLogin = true;
     $http.post('../session/login-exec.php', user).success(function(response) {
     if (response == "success") {
     alert("Logged in successfully.");
     window.location.href = "/index.php";
     } else {
     alert(response);
     $scope.isProcessingLogin = false;
     $scope.errorMessage = response;
     }
     }).error(function(data, status) {
     $scope.isProcessingLogin = false;
     $scope.errorMessage = status;
     alert(data + status);
     });
     };
     */
}]);
