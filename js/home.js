function onLoad() {
    var inputCurrentURL = document.getElementsByName('inputCurrentURL');
    var i;
    for ( i = 0; i < inputCurrentURL.length; i++) {
        inputCurrentURL[i].value = getCurrentURL();
    }
}

function getCurrentURL() {
    return window.location.href;
}
