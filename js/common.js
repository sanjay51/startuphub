<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        
        <!-- Include the Bootstrap library -->
        <link href="libs/bootstrap.min.css" rel="stylesheet" />
        <script src="libs/jquery.min.js"></script>
        <script src="libs/bootstrap.min.js"></script>

        <!-- Include the AngularJS library --> 
        <script src="libs/angular.min.js"></script>
        
        <script type="text/javascript" src="/js/home.js"></script>
        <link rel="stylesheet" type="text/css" href="/style.css" />