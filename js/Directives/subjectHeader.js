startupHubApp.directive('subjectHeader', function() {
    return {
        restrict : 'E',
        scope : {
            subjectname : '=subjectname',
            subjectimage : '=subjectimage',
            subjectuserid : '=subjectuserid'
        },
        templateUrl : 'js/Directives/subjectHeader.php'
    };

});
