startupHubApp.directive('post', function() {
    return {
        restrict: 'E',
        scope: {
            content: '=content'
        },
        templateUrl: 'js/Directives/post.php'
    };
});
