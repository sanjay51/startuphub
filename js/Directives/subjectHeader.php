<div ng-controller="HoverProfileLoaderController" ng-mouseover="loadHoverProfile(subjectuserid);">
    <div ng-mouseover="showHoverProfile = true" ng-mouseout="showHoverProfile = false" style="display:inline-block" >
        <a href="#"> <img width=30 height=30 ng-src="{{ subjectimage }}" /><label>{{ subjectname }}</label> </a>
    </div>
    <div class="hoverProfile" ng-show="showHoverProfile" ng-mouseover="showHoverProfile = true;" ng-mouseout="showHoverProfile = false; isProfileLoaded = false">
        <div class="arrow-up"></div>
        <div class="hoverProfileData">
            <div class="row">
                <div class="col-sm-5">
                    <img class="imgHoverDP" width=120 height=120 ng-src="{{ $parent.subjectimage }}" />
                </div>
                <div class="col-sm-7">
                    <label>{{ subjectname }}</label>
                    <label>{{ data.tagline }}</label>
                    <br/>
                    <label>{{ data.line2 }}</label>
                    <br/>
                    <label>{{ data.line3 }}</label>
                    <br/>
                    <img ng-show="isProfileLoaded" src="images/loading2.gif" />
                </div>
            </div>

        </div>
    </div>
</div>

<style>
    .arrow-up {
        width: 0;
        height: 0;
        border-left: 20px solid transparent;
        border-right: 20px solid transparent;
        border-bottom: 20px solid #96AAF8;
        margin-left: 20px;
        margin-bottom: -5px;
    }

    .hoverProfile {
        position: absolute;
    }

    .hoverProfileTopImage {
        margin-bottom: -8px;
        z-index: 99;
    }

    .hoverProfileData {
        width: 300px;
        box-shadow: 5px 5px 10px 1px #337AB7;
        background-color: #D9F7E5;
        border-color: #96AAF8;
        border-width: 2px;
        border-style: solid;
        border-radius: 10px;
    }

    .imgHoverDP {
        border-radius: 10px;
    }
</style>

