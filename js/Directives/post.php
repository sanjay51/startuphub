<?php
include $_SERVER[DOCUMENT_ROOT].'/db/db_connect.php';
?>
<div class="divPost">
    <div class="divPostHeading">
        <subject-header subjectuserid="content.creator_user_id" subjectname="content.name" subjectimage="content.image_link"></subject-header>
        <br/>
        <label>{{ content.text }}</label>
    </div>
    <div class="divPostOptions">
        <Button ng-disabled="isDisabled" ng-click="isDisabled = $parent.askForResume(content.creator_user_id, content.post_id)" ng-init="isDisabled=false">
            Ask for resume
        </Button>
        <Button ng-click="$parent.sendMessage(content.creator_user_id, content.post_id, content.name)">
            Send message
        </Button>
        <Button>
            Report
        </Button>
    </div>

    <div id="divPostComments" class="divPostComments">
        <div ng-repeat="comment in content.comments">
            <comment content = "comment"></comment>
        </div>
    </div>
    
    <div class="divPostReply">
        <img src="<?php echo getSessionUserImage(); ?>" width="25" height="25">
        <input ng-model="reply1" type="text" placeholder="Type a reply.." 
            ng-keypress='$parent.addReplyToScope($event, content, reply1, "<?php echo getSessionUserID(); ?>" , "<?php echo getSessionDisplayName(); ?>", "<?php echo getSessionUserImage(); ?>" )' style="width:90%">
    </div>
</div>