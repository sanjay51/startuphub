startupHubApp.directive('appHeader', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        templateUrl:'/js/Directives/header.php'
    };
});
