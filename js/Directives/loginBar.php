<?php
include $_SERVER['DOCUMENT_ROOT'] . '/db/db_connect.php';
?>
<?php if (isLoggedIn()) {
?>
<userdropdown username ="'<?php echo getSessionDisplayName(); ?>'"></userdropdown>
<?php
} else {
?>
<div class="row">
    <div align="right" class="col-sm-12">
        <div class="btn btn-danger" style="color:white">
            <a style="color:white" href="/pages/login.php">Login</a>
        </div>
        <a style="color:black" href="/pages/register.php">Register</a>
    </div>
</div>
<?php
}
?>