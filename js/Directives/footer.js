startupHubApp.directive('footer', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        templateUrl:'/js/Directives/footer.php'
    };
});
