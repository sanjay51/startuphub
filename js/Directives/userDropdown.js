startupHubApp.directive('userdropdown', function() {
    return {
        restrict: 'E',
        scope: {
            username: '='
        },
        templateUrl: 'js/Directives/userDropdown.php'
    };
});
