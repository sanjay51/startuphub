<li class="dropdown pull-right">
    <button style="margin-right:10px;" type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">{{username}} <b class="caret"></b></button>
    <ul class="dropdown-menu">
        <li>
            <a href="#">Action</a>
        </li>
        <li>
            <a href="#">Another action</a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="session/logout.php">Logout</a>
        </li>
    </ul>
</li>