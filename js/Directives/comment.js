startupHubApp.directive('comment', function() {
    return {
        restrict: 'E',
        scope: {
            content: '='
        },
        templateUrl: 'js/Directives/comment.php'
    };
});
