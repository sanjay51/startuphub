startupHubApp.directive('filterSidebar', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        templateUrl:'/js/Directives/filterSidebar.php'
    };
});
