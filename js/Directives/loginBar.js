startupHubApp.directive('loginBar', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        templateUrl:'/js/Directives/loginBar.php'
    };
});
