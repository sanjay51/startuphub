<?php
  include $_SERVER['DOCUMENT_ROOT'] . '/db/queries/getMessageThreadByIDQuery.php';
  $thread = json_decode(file_get_contents("php://input"));
  
  if (isset($_GET["thread_id"])) {
      $thread->thread_id = $_GET["thread_id"];
  }
  
  //TODO: Validate user data here..
  
  if (! isset($thread->thread_id)) {
      echo getMessageThreadIDByToAndPostID($thread);
  } else {
      echo getMessageThreadByIDJSON($thread);
  }
?>