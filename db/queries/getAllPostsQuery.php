<?php
include $_SERVER[DOCUMENT_ROOT].'/db/db_connect.php';
include $_SERVER[DOCUMENT_ROOT].'/elements/html/getDivPost.php';
include $_SERVER[DOCUMENT_ROOT].'/db/queries/getPostRepliesQuery.php';

function getAllPosts() {
    $query = "SELECT * FROM  `posts` LIMIT 0 , 10";

    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            $post_id = $row["post_id"];
            $name = $row["creator_name"];
            $image_link = $row["creator_image_link"];
            $text = $row["description"];
            $ret = $ret.getDivPost($post_id, $name, $image_link, $text).getPostReplies($post_id);
        }
    }
    
    return $ret;
}

function getAllPostsJSON() {
    $query = "SELECT posts.description, posts.description, posts.status, posts.timestamp, posts.post_id, users.display_name, users.image, users.user_id FROM `posts` left join users on posts.creator_user_id=users.user_id LIMIT 0 , 10;";

    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            if ($ret != "") {
                $ret .= ",";
            }
            
            $post_id = $row["post_id"];
            $name = $row["display_name"];
            $creator_user_id = $row["user_id"];
            $image_link = $row["image"];
            $text = $row["description"];
            
            
            $ret .= "{ \"comments\": ".getPostRepliesJSON($post_id).",  \"creator_user_id\": \"".$creator_user_id."\",  \"post_id\": \"".$post_id."\", \"name\": \"".$name."\", \"image_link\": \"".$image_link."\", \"text\":\"".$text."\" }";
        }
    }
    $ret ='{"records":['.$ret.']}';
    return $ret;
}
