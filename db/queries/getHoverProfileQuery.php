<?php
include $_SERVER[DOCUMENT_ROOT] . '/db/db_connect.php';

function getHoverProfile($user) {
    $user_id = $user -> user_id;

    $query = "SELECT account_type FROM `users` WHERE user_id=$user_id;";

    $db = getDBConnection();
    $result = $db -> query($query);

    $account_type = $result -> fetch_assoc() ["account_type"];
    
    if ($account_type == "student") {
        $query = "SELECT 'Student' as tagline, qualification as line2, college as line3 FROM `student_profile` WHERE parent_id=$user_id;";
    } else {
        $query = "SELECT oneliner as tagline, website as line2, country as line3 FROM `company_profile` WHERE parent_id=$user_id;";
    }
    
    $result = $db -> query($query);

    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            $ret = $row;
        }
    }

    return json_encode($ret);
}
?>