<?php
include $_SERVER[DOCUMENT_ROOT].'/db/db_connect.php';

function getUserProfile() {
    $accountType = getAccountType();
    
    if ($accountType == "student") {
        return getStudentProfile();
    } else if ($accountType == "company") {
        return getCompanyProfile();
    }
}

function getStudentProfile() {
    $user_id = $_SESSION['USER_ID'];
    
    $query = "SELECT * FROM `student_profile` WHERE parent_id=$user_id;";

    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            $ret = $row;
        }
    }
    
    return json_encode($ret);
}

function getCompanyProfile() {
    $user_id = $_SESSION['USER_ID'];
    
    $query = "SELECT * FROM `company_profile` WHERE parent_id=$user_id;";

    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            $ret = $row;
        }
    }
    
    return json_encode($ret);
}
?>