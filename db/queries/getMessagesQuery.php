<?php
include $_SERVER[DOCUMENT_ROOT].'/db/db_connect.php';
include $_SERVER[DOCUMENT_ROOT].'/elements/html/getDivPost.php';
include $_SERVER[DOCUMENT_ROOT].'/db/queries/getPostRepliesQuery.php';

function getMessagesJSON() {
    
    $to_user_id = getSessionUserID();
    
    //TODO: Change limit and support pagination
    //$query = "SELECT * FROM  `messages` where to_user_id='$to_user_id' LIMIT 0 , 50"; 
    
    $query = "SELECT message_threads.thread_id, message_threads.from_user_id, message_threads.subject, message_threads.message, message_threads.related_post_id, ".
             "message_threads.read_status, users.image, users.display_name ".
             "FROM  `message_threads`  ".
             "LEFT JOIN  `users` ON message_threads.from_user_id = users.user_id ".
             "WHERE message_threads.to_user_id = '$to_user_id' ".
             "LIMIT 0 , 30;";

    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        while ($row = $result -> fetch_assoc()) {
            if ($ret != "") {
                $ret .= ",";
            }
            
            $thread_id = $row["thread_id"];
            $from_user_id = $row["from_user_id"];
            $subject = $row["subject"];
            $message = $row["message"];
            $related_post_id = $row["related_post_id"];
            $read_status = $row["read_status"];
            $sender_name = $row["display_name"];
            $sender_image_link = $row["image"];
            
            $ret .= "{ ".
                       " \"message_id\": \"$thread_id\",  ".
                       " \"sender_name\": \"$sender_name\",  ".
                       " \"sender_image_link\": \"$sender_image_link\",  ".
                       " \"from_user_id\": \"$from_user_id\",  ".
                       " \"subject\": \"$subject\",  ".
                       " \"message\": \"$message\",  ".
                       " \"related_post_id\": \"$related_post_id\",  ".
                       " \"read_status\": \"$read_status\"  ".
                     "}";
        }
    }
    
    $ret = '{ "messages": ['.$ret.']}';
    return $ret;
}
