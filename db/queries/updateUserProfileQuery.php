<?php
include $_SERVER[DOCUMENT_ROOT] . '/db/db_connect.php';

function updateCompanyProfile($company) {
    $user_id = $_SESSION['USER_ID'];

    if (hasCompletedProfile()) {
        // => Update profile request
        $query = "UPDATE company_profile set" . " `oneliner`='$company->oneliner',
         `contact`='$company->contact',
         `website`='$company->website',
         `country`='$company->country',
         `domain`='$company->domain',
         `about`='$company->about'
         WHERE `parent_id`='$user_id'";

        $db = getDBConnection();
        $result = $db -> query($query);
    } else {
        // => Create profile request
        $query = "INSERT INTO `company_profile`
        (`parent_id`, `oneliner`, `contact`,
         `website`, `country`, `domain`,
           `about`, `creation_date`)
               VALUES 
               ('$user_id', '$company->oneliner', '$company->contact',
               '$company->website', '$user->country', '$user->domain',
               '$user->about', CURRENT_TIMESTAMP)
               ";

        $query2 = "UPDATE users set `has_completed_profile`='Y' where
               user_id='$user_id'";

        $db = getDBConnection();
        $result = $db -> query($query);
        $result = $db -> query($query2);

    }

    $_SESSION['HAS_COMPLETED_PROFILE'] = 'Y';
    echo "success";
}

function updateUserProfile($user) {

    $user_id = $_SESSION['USER_ID'];

    if (hasCompletedProfile()) {
        // => Update profile request

        $query = "UPDATE student_profile set" . " `name`='$user->name',
         `qualification`='$user->qualification',
         `current_year`='$user->present_year',
         `college`='$user->college',
         `branch`='$user->stream',
         `other_personal_details`='$user->other_personal_details',
         `academic_details`='$user->other_academic_details',
         `objective`='$user->objective',
         `projects`='$user->projects',
         `achievements`='$user->achievements',
         `other_activities`='$user->other_activities',
         `skills`='$user->skills',
         `academic_intersts='$user->academic_interests',
         `other_interests`='$user->other_interests',
         `other_details`='$user->other_details',
         `contact`='$user->contact'
         WHERE `parent_id`='$user_id'";

        $db = getDBConnection();
        $result = $db -> query($query);
    } else {
        // => Create profile request
        $query = "INSERT INTO `student_profile`
        (`parent_id`, `name`, `qualification`,
         `current_year`, `college`, `branch`,
          `other_personal_details`, `academic_details`,
           `objective`, `projects`, `achievements`,
            `other_activities`, `skills`, `academic_interests`,
             `other_interests`, `other_details`, `contact`)
               VALUES 
               ('$user_id', '$user->name', '$user->qualification',
               '$user->present_year', '$user->college', '$user->stream',
               '$user->other_personal_details', '$user->other_academic_details',
               '$user->objective', '$user->projects', '$user->achievements', 
               '$user->other_activities', '$user->skills', '$user->academic_interests',
               '$user->other_interests', '$user->other_details', '$user->contact')
               ";

        $query2 = "UPDATE users set `has_completed_profile`='Y' where
               user_id='$user_id'";

        $db = getDBConnection();
        $result = $db -> query($query);
        $result = $db -> query($query2);

    }

    $_SESSION['HAS_COMPLETED_PROFILE'] = 'Y';
}
?>