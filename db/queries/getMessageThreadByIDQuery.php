<?php
include $_SERVER[DOCUMENT_ROOT].'/db/db_connect.php';

function getMessageThreadIDByToAndPostID($message) {
    $from_user_id = getSessionUserID();
    $to_user_id = $message->to_id;
    $related_post_id = $message->post_id;
    
    $query = "SELECT thread_id
              FROM message_threads
              WHERE related_post_id =3
              AND (
                (
                    to_user_id =7
                    AND from_user_id =2
                )
                OR (
                    to_user_id =2
                    AND from_user_id =7
                )
              );";
    
    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows == 1) {
        $row = $result -> fetch_assoc();
        
        $ret = '{ "thread_id" : "'.$row["thread_id"].'" }';
    }
    
    return $ret;
}

function getMessageThreadByIDJSON($thread) {
    
    $to_user_id = getSessionUserID();
    
    //TODO: Change limit and and pagination
    
    $query = "SELECT messages.message_serial, messages.sender_user_id, users.image as sender_image_link, messages.body, messages.timestamp, ".
             "message_threads.to_user_id, message_threads.from_user_id,message_threads.subject, message_threads.related_post_id ".
             "FROM message_threads ".
             "LEFT JOIN messages ON message_threads.thread_id = messages.thread_id ".
             "LEFT JOIN users ON messages.sender_user_id = users.user_id ".
             "WHERE message_threads.thread_id = '$thread->thread_id' ".
             "ORDER BY messages.message_serial DESC  ".
             "LIMIT 0 , 30;";

    //echo $query;
    
    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        while ($row = $result -> fetch_assoc()) {
            if ($ret != "") {
                $ret .= ",";
            }
            
            $message_serial = $row["message_serial"];
            $sender_user_id = $row["sender_user_id"];
            $sender_image_link = $row["sender_image_link"];
            $body = $row["body"];
            $to_user_id = $row["to_user_id"];
            $from_user_id = $row["from_user_id"];
            $subject = $row["subject"];
            $related_post_id = $row["related_post_id"];
            $timestamp = $row["timestamp"];
            
            $ret .= "{
                 \"message_serial\": \"$message_serial\", 
                 \"sender_user_id\": \"$sender_user_id\", 
                 \"sender_image_link\": \"$sender_image_link\",
                 \"body\": \"$body\", 
                 \"to_user_id\": \"$to_user_id\", 
                 \"from_user_id\": \"$from_user_id\", 
                 \"subject\": \"$subject\", 
                 \"related_post_id\": \"$related_post_id\",
                 \"timestamp\": \"$timestamp\"
                 }";
        }
    }
    
    $ret ='{"messages":['.$ret.']}';
    return $ret;
}
