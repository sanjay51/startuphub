<?php
include $_SERVER[DOCUMENT_ROOT].'/elements/html/getDivPostReply.php';

function getPostRepliesJSON($post_id) {
    $query = "SELECT replies.id, users.display_name as creator_name, users.image as creator_image_link, replies.reply_text, replies.parent_id, replies.status FROM  replies left join users on replies.creator_user_id = users.user_id where replies.parent_id='1' LIMIT 0 , 30";
    
    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            if ($ret != "") {
                $ret .= ",";
            }
            
            $reply_id = $row["id"];
            $creator_name = $row["creator_name"];
            $creator_image_link = $row["creator_image_link"];
            $reply_text = $row["reply_text"];
            
            $ret .= "{ \"reply_id\": \"$reply_id\", \"creator_name\" : \"$creator_name\", \"creator_image_link\": \"$creator_image_link\", \"reply_text\": \"$reply_text\"  }";
        }
    }
    $ret = "[".$ret."]";
    return $ret;
}

function getPostReplies($post_id) {
    $query = "SELECT * FROM  `replies` where parent_id='$post_id' LIMIT 0 , 30";
    
    $db = getDBConnection();
    $result = $db -> query($query);
    
    $ret = "";
    if ($result -> num_rows > 0) {
        // output data of each row
        while ($row = $result -> fetch_assoc()) {
            $reply_id = $row["id"];
            $creator_name = $row["creator_name"];
            $creator_image_link = $row["creator_image_link"];
            $reply_text = $row["reply_text"];
            $ret = $ret.getDivPostReply($reply_id, $creator_name, $creator_image_link, $reply_text);
        }
    }
    
    return $ret;
}
