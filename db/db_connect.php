<?php
session_start();
function isLoggedIn() {
    if (isset($_SESSION['USER_ID'])) {
        return true;
    }
    return false;
}

function getAccountType() {
    return $_SESSION['ACCOUNT_TYPE'];
}

function is_compnay() {
    return (getAccountType() == "company");
}

function ensure_logged_in() {
    if (isLoggedIn())
    return;
    
    header("location: ../session/access-denied.php");
}

function ensure_company_logged_in() {
    if (isLoggedIn() && is_compnay())
    return;
    
    header("location: ../session/access-denied.php");
}

function ensure_not_logged_in() {
    if (! isLoggedIn())
    return;
    
    header("location: ../index.php");
}

function getSessionDisplayName() {
    return $_SESSION['DISPLAY_NAME'];
}

function getSessionUserImage() {
    return $_SESSION['IMAGE'];
}

function getSessionUserID() {
    return $_SESSION['USER_ID'];
}

function hasCompletedProfile() {
    return isLoggedIn() && (strcmp($_SESSION['HAS_COMPLETED_PROFILE'], 'Y') == 0 ? TRUE : FALSE);
}

function getDBConnection() {
    static $db = null;

    if ($db != null) {
        return $db;
    }
    
    //$servername = "fdb2.awardspace.com";
    //$username = "1066258_pico";
    //$port = 3306;
    //$password = "Haryana1*";
    //$dbname = $username;

    //Try local db settings
    $servername = "localhost";
    $username = "root";
    $port = 3306;
    $password = "haryana";
    $dbname = "1066258_pico";
    
    // Create connection
    $db = new mysqli($servername, $username, $password, $dbname, $port);

    // Check connection
    if ($db -> connect_error) {
        die("Connection error with database. Please try again :(");
    }

    return $db;

}
