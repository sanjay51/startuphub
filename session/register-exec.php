<?php
//Start session
session_start();

//Include database connection details
include $_SERVER[DOCUMENT_ROOT] . '/db/db_connect.php';
include $_SERVER[DOCUMENT_ROOT] . '/db/queries/getUserByEmailQuery.php';
include $_SERVER[DOCUMENT_ROOT] . '/db/queries/addNewUserQuery.php';

//Array to store validation errors
$errmsg_arr = array();

//Validation error flag
$errflag = false;

//Select database
$db = getDBConnection();

//Function to sanitize values received from the form. Prevents SQL injection
function clean($str) {
    $str = @trim($str);
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return $str;
}

$user = json_decode(file_get_contents("php://input"));

//Sanitize the POST values
$displayName = clean($user -> display_name);
$legalName = clean($user -> legal_name);
$email = clean($user -> email);
$password = clean($user -> password);
$account_type = clean($user -> account_type);
$cpassword = clean($user -> confirm_password);

//Input Validations
if ($displayName == '') {
    $errmsg_arr[] = 'Display name cannot be left blank.';
    $errflag = true;
} else if (strlen($displayName) < 5) {
    $errmsg_arr[] = 'Display name must contain atleast 5 characters.';
    $errflag = true;
}

if ($legalName == '') {
    $errmsg_arr[] = 'Legal name cannot be left blank.';
    $errflag = true;
} else if (strlen($legalName) < 5) {
    $errmsg_arr[] = 'Legal name must contain atleast 5 characters.';
    $errflag = true;
}

if ($email == '') {
    $errmsg_arr[] = 'Email is required';
    $errflag = true;
} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailErr = "Please enter a valid email.";
}

if ($password == '') {
    $errmsg_arr[] = 'Password missing';
    $errflag = true;
} else if (strlen($password) < 7) {
    $errmsg_arr[] = 'Password should contain atleast 7 characters';
    $errflag = true;
}

if ($cpassword == '') {
    $errmsg_arr[] = 'Confirm password missing';
    $errflag = true;
}

if (strcmp($password, $cpassword) != 0) {
    $errmsg_arr[] = 'Passwords do not match';
    $errflag = true;
}

//Check for duplicate email ID
if ($email != '') {
    if (doesEmailAlreadyExist($email)) {
        $errmsg_arr[] = 'Email ID already in use.';
        $errflag = true;
    }
}

//If there are input validations, redirect back to the registration form
if ($errflag) {
    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
    echo json_encode($errmsg_arr);
    session_write_close();
    exit();
}

//Add new user
$result = addNewUser($displayName, $legalName, $email, $account_type, $password);

//Check whether the query was successful or not
if ($result) {
    echo "success";
    exit();
} else {
    echo "Failed to register at this time. Internal error. Please try again.";
}
?>