<?php
include $_SERVER['DOCUMENT_ROOT'] . '/db/db_connect.php';

//Validation error flag
$errflag = false;

$db = getDBConnection();

//Function to sanitize values received from the form. Prevents SQL injection
function clean($str) {
    $str = @trim($str);
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    //return mysql_real_escape_string($str);
    return $str;
}

$user = json_decode(file_get_contents("php://input"));

//Sanitize the POST values
$email = clean($user -> email);
$password = clean($user -> password);

//Input Validations
if ($email == '') {
    $errflag = true;
} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errflag = true;
}

if ($password == '') {
    $errflag = true;
} else if (strlen($password) < 7) {
    $errflag = true;
}

//If there are input validations, redirect back to the login form
if ($errflag) {
    $errmsg = "Invalid email | password.";
    session_write_close();
    echo json_encode($errmsg);
    exit();
}

//Create query
$query = "SELECT * FROM users WHERE email='$email' AND password='$password'";
$result = $db -> query($query);

//Check whether the query was successful or not
if ($result) {
    if ($result -> num_rows == 1) {
        //Login Successful  

        $user = $result -> fetch_assoc();

        //Start session
        session_start();
        //session_regenerate_id();

        $_SESSION['USER_ID'] = $user['user_id'];
        $_SESSION['DISPLAY_NAME'] = $user['display_name'];
        $_SESSION['HAS_COMPLETED_PROFILE'] = $user['has_completed_profile'];
        $_SESSION['ACCOUNT_TYPE'] = $user['account_type'];
        $_SESSION['IMAGE'] = $user['image'];

        echo "success";
    } else {
        echo "No account found with that username and password. Please register.";
        exit();
    }
} else {
    die("Query failed");
}
?>