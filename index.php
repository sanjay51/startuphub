<?php
include $_SERVER['DOCUMENT_ROOT'] . '/db/db_connect.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER['DOCUMENT_ROOT'] . '/js/common.js';
        ?>
    </head>
    <body onload="onLoad()" ng-app="StartupHubApp">

        <div ng-controller='HeaderController'>
            <app-header></app-header>
        </div>

        <div class="container" style="margin-left: 0px">
            <div class="row">
                <div class="col-sm-2" style="background-color:#cdcdf7;">
                    <filter-sidebar></filter-sidebar>
                </div>

                <div class="col-sm-10">
                    <div class="divAllPosts" ng-controller="PostsController">
                        <button class="btn btn-success" type="button" ng-click="createPost()">
                            + Create new post
                        </button>
                        <br/>

                        <div ng-repeat="post in posts">
                            <post content="post"></post>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
        <div class="row">
            <footer></footer>
        </div>
        <div id="divLog">
            Logs here
        </div>
            
        </div>
        <!-- Modules -->
        <script src="js/StartupHubApp.js"></script>

        <!-- Controllers -->
        <script src="js/Controllers/HeaderController.js"></script>
        <script src="js/Controllers/PostsController.js"></script>
        <script src="js/Controllers/HoverProfileLoaderController.js"></script>

        <!-- Directives -->
        <script src="js/Directives/header.js"></script>
        <script src="js/Directives/loginBar.js"></script>
        <script src="js/Directives/filterSidebar.js"></script>
        <script src="js/Directives/footer.js"></script>
        <script src="js/Directives/post.js"></script>
        <script src="js/Directives/comment.js"></script>
        <script src="js/Directives/userDropdown.js"></script>
        <script src="js/Directives/subjectHeader.js"></script>

    </body>
</html>