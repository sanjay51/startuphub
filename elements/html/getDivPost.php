<?php
function getSampleDivPost() {
    return getDivPost("Studds Ninja", "http://graph.facebook.com/sanjay.verma.nitk/picture?type=square", "This is a post");
}

function getDivPost($post_id, $name, $image_link, $text) {
    return <<<HTML
    <div class="divPost">
                <div class='divPostHeading'>
                    <img width=30 height=30 src='$image_link' />
                    <label>$name</label>
                    <br/>
                    <label>$text</label>
                </div>
                <div class='divPostOptions'>
                    <Button>
                        Upvote
                    </Button>
                    <Button>
                        Comment
                    </Button>
                    <Button>
                        Subscribe
                    </Button>
                    <Button>
                        Share
                    </Button>
                </div>
                
                <form action="actions/replyToPost.php">
                    <input type="text" size=50 name="reply_text" placeholder="reply here.." />
                    <input type="hidden" name="post_id" value='$post_id' />
                    <input type="hidden" name="inputCurrentURL" />
                    <input type="submit" value="Reply">
                </form>
            </div>
HTML;
}
?>