<?php

function getSampleDivPostReply() {
    return getDivPost(1, "Joe Raymen", "http://graph.facebook.com/sanjay.verma.nitk/picture?type=square", "Can you please PM me the details?");
}

function getDivPostReply($reply_id, $creator_name, $creator_image_link, $reply_text) {
    if ($creator_image_link == null) {
        $creator_image_link = "http://graph.facebook.com/sanjay.verma.nitk/picture?type=square";
    }
    
    return <<<HTML
    <div class="divPostReply">
      <div class='divComment'>
        <img width=20 height=20 src='$creator_image_link' />
        <label>$creator_name</label>
        <label>$reply_text</label>
      </div>
    </div>
HTML;
}
?>