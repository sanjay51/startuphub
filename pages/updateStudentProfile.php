<?php
include '../db/db_connect.php';

ensure_logged_in();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER['DOCUMENT_ROOT'] . '/js/common.js';
        ?>
    </head>

    <body ng-app="StartupHubApp">
        <div ng-controller='HeaderController'>
            <app-header></app-header>
            <login-bar></login-bar>
        </div>

        <div class="container" ng-controller="StudentFormValidationController">
            <form name="form" role="form" class="form-horizontal" novalidate>
                <div class="form-group">
                    <div class="row">
                        <label class="control-label col-sm-2"  for="name">Name(*):</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="uname" name="uname" required="" ng-model="user.name" placeholder="Enter your full name" ng-minlength="5" />
                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.uname.$error.minlength">Name should contain atleast 5 characters.</span>
                            <span ng-show="form.uname.$error.required">Name cannot be blank.</span>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="contact" class="control-label col-sm-2">Contact(*):</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control col-sm-6" required="" name="contact" ng-model="user.contact" placeholder="Your contact number" ng-minlength="8" />
                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.contact.$error.minlength">Contact number should be of atleast 8 digits.</span>
                            <span ng-show="form.contact.$error.required">Contact number cannot be left blank.</span>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="qualification" class="control-label col-sm-2">Qualification(*):</label>
                        <div class="col-sm-6">
                            <select class="form-control" ng-model="user.qualification" name="qualification">
                                <option>B.E./B.Tech</option>
                                <option>M.E./M.Tech</option>
                                <option>B.Tech+M.Tech (dual degree)</option>
                                <option>M.S.</option>
                                <option>B.Sc.</option>
                                <option>M.Sc.</option>
                                <option>BCA</option>
                                <option>MCA</option>
                            </select>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="present_year" class="control-label col-sm-2">Present year(*):</label>
                        <div class="col-sm-6">
                            <select class="form-control" ng-model="user.present_year" name="present_year">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>Already graduated</option>
                            </select>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="college" class="control-label col-sm-2">College(*):</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" ng-model="user.college" name="ucollege" required="" id="ucollege" placeholder="Enter college name here (abbreviated, without dots). e.g. NIT Rourkela" />
                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.ucollege.$error.required">College name cannot be left blank.</span>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="stream" class="control-label col-sm-2">Stream(*): </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" ng-model="user.stream" required="" id="ustream" name="ustream" placeholder="e.g. ECE, CSE, Mechanical etc." />
                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.ustream.$error.required">Stream(branch) cannot be left blank.</span>
                        </div>
                    </div>
                    <br/>
                    <br/>

                    <div class="row">
                        <label for="other_personal_details" class="control-label col-sm-2">Other personal details:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="5" ng-model="user.other_personal_details"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="other_academic_details">Other academic details:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.other_academic_details" rows="5" placeholder="e.g. Got 89% in Secondary school exams." id="other_academic_details"></textarea>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="form-group">

                    <div class="row">
                        <label class="control-label col-sm-2" for="objective">Objective:</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="name" ng-model="user.objective" class="form-control" id="objective" placeholder="e.g. Looking for 6 months internship in a growing startup." />
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="skills">Skills(*):</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.skills" rows="5" placeholder="e.g. Java(Intermediate), MySQL(beginner)" id="skills"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="projects">Projects:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.projects" rows="5"  placeholder="..a brief mention of projects that you have completed." id="projects"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="achievements">Achievements:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.achievements" rows="5" placeholder="..a brief mention of your achievements from your past." id="achievements"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="other_activities">Extra-curricular activities:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.other_activities" rows="5" placeholder=".. a brief mention of extra-curricular activities that you are/were a part of" id="other_activities"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="academic_interests">Academic interests:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.academic_interests" rows="5" placeholder=".. a mention of subjects that you were most interested in, and may want to pursue future career, e.g. Cryptography, Image processing, Discrete Mathematics etc." id="academic_interests"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="other_interests">Other interests/hobbies:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.other_interests" rows="5" placeholder=".. other interests/hobbies that you would like to mention" id="other_interests"></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label class="control-label col-sm-2" for="other_details">Other details:</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="user.other_details" rows="5" placeholder=".. any other details not captured above" id="other_details"></textarea>
                        </div>
                    </div>
                    <br/>
                </div>

                <button type="submit" ng-click="update(user)" class="btn btn-default">
                    Save profile!
                </button>
                <a href="/">Go back</a>
            </form>
        </div>

        <style type="text/css">
            .form-horizontal input.ng-invalid.ng-dirty {
                border-color: #FA787E;
            }

            .form-horizontal input.ng-valid.ng-dirty {
                border-color: #78FA89;
            }

            .light-heading h2 {
                font-size: 20px;
                opacity: 0.5;
            }
        </style>

        <!-- Modules -->
        <script src="/js/StartupHubApp.js"></script>

        <!-- Controllers -->
        <script src="/js/Controllers/HeaderController.js"></script>
        <script src="/js/Controllers/StudentFormValidationController.js"></script>

        <!-- Directives -->
        <script src="/js/Directives/header.js"></script>
        <script src="/js/Directives/loginBar.js"></script>
    </body>
</html>