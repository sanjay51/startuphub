<?php
include '../db/db_connect.php';

ensure_not_logged_in();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER[DOCUMENT_ROOT] . '/js/common.js';
        ?>
        <title>Create an account..</title>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body ng-app="StartupHubApp">
        <div ng-controller='HeaderController'>
            <app-header></app-header>
            <login-bar></login-bar>
        </div>

        <div class="container" ng-controller="RegistrationController">
            <div class="page-header">
                <h1>Register <small>Create an account</small></h1>
            </div>
            <div class="error-message" ng-repeat="r in errorMessage">
                {{r}}
            </div>
            <form class="form-horizontal" name="registerForm" method="post" novalidate>
                <div class="row">
                    <label class="control-label col-sm-5">Display Name (will be visible publically) </label>
                    <div class="col-sm-4">
                        <input class="form-control" required="" name="displayName" ng-model="user.display_name" ng-minlength="5" type="text" class="textfield" id="displayName" />
                        <div ng-show="registerForm.displayName.$error.required" class="error-message">
                            Display name cannot be left blank.
                        </div>
                        <div ng-show="registerForm.displayName.$error.minlength" class="error-message">
                            Display name must contain atleast 5 characters.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Legal Name</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="legalName" ng-model="user.legal_name" type="text" class="textfield" required="" ng-minlength="5" />

                        <div ng-show="registerForm.legalName.$error.required" class="error-message">
                            Legal name cannot be left blank.
                        </div>
                        <div ng-show="registerForm.legalName.$error.minlength" class="error-message">
                            Legal name must contain atleast 5 characters.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Email</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="email" ng-model="user.email" required="" type="email" class="textfield" id="email" />
                        <div ng-show="registerForm.email.$error.required" class="error-message">
                            Email is required.
                        </div>
                        <div ng-show="registerForm.email.$error.email" class="error-message">
                            Please enter a valid email.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Account type</label>
                    <div class="col-sm-4">
                        <select ng-init="user.account_type='student'" ng-model="user.account_type" class="form-control selectpicker show-tick pull-right" name="account_type">
                            <option value="student">Student</option>
                            <option value="professional">Professional</option>
                            <option value="company">Company</option>
                        </select>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Password</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="password" ng-model="user.password" type="password" class="textfield" required="" ng-minlength="7" />
                    <div ng-show="registerForm.password.$error.minlength" class="error-message">
                        Password should contain atleast 7 characters.
                    </div>
                    <div ng-show="registerForm.password.$error.required" class="error-message">
                        Must enter a password.
                    </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Confirm Password</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="cpassword" ng-model="user.confirm_password" type="password" class="textfield" id="cpassword" />
                        <div ng-show="user.password != user.confirm_password" class="error-message">
                            Passwords do not match.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <p align="center">
                        <div class="g-recaptcha" align="center" data-sitekey="6LdMkgUTAAAAADwaXcMGVktngZaL-L289UHi7aoa"></div>
                    </p>
                </div>

                <div class="row">
                    <p align="center">
                        <input ng-show="isProcessingRegistration == false" class="btn btn-large btn-primary" ng-click="register(user)" type="submit" name="Submit" value="Register" />
                        <input class="btn btn-large btn-primary" ng-show="isProcessingRegistration == true" value="Please wait.." />
                    </p>
                </div>
            </form>
        </div>

        <style type="text/css">
            .center_div {
                margin: 0 auto;
                width: 100%;
            }
            .error-message {
                font-size: 10px;
                color: red;
            }
        </style>

        <!-- Modules -->
        <script src="/js/StartupHubApp.js"></script>

        <!-- Controllers -->
        <script src="/js/Controllers/HeaderController.js"></script>
        <script src="/js/Controllers/RegistrationController.js"></script>

        <!-- Directives -->
        <script src="/js/Directives/header.js"></script>
        <script src="/js/Directives/loginBar.js"></script>
    </body>
</html>