<?php
include '../db/db_connect.php';

ensure_not_logged_in();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER['DOCUMENT_ROOT'] . '/js/common.js';
        ?>

        <title>Login</title>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>

    <body ng-app="StartupHubApp">
        <div ng-controller='HeaderController'>
            <app-header></app-header>
        </div>

        <div class="container" ng-controller="LoginController">
            <div class="page-header">
                <h1>Login <small>authenticate yourself!</small></h1>
            </div>
            <form class="form-horizontal" name="loginForm" method="post" novalidate>
                <div class="row">
                    <label class="control-label col-sm-5">Email </label>
                    <div class="col-sm-4">
                        <input required="" ng-model="user.email" name="email" type="email" class="form-control" />
                        <div ng-show="loginForm.email.$error.required" class="error-message">
                            Email cannot be left blank.
                        </div>
                        <div ng-show="loginForm.email.$error.email" class="error-message">
                            Please enter a valid email.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Password </label>
                    <div class="col-sm-4">
                        <input name="password" ng-model="user.password" type="password" class="form-control" />
                        <div ng-show="registerForm.password.$error.required" class="error-message">
                            Please enter a password.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <p align="center">
                        <input ng-show="isProcessingLogin == false" class="btn btn-large btn-primary" ng-click="login(user)" type="submit" name="submit" value="Login" />
                        <input class="btn btn-large btn-primary" ng-show="isProcessingLogin == true" value="Please wait.." />
                    </p>
                </div>
            </form>

            <style type="text/css">
                .center_div {
                    margin: 0 auto;
                    width: 100%;
                }
                .error-message {
                    font-size: 10px;
                    color: red;
                }
            </style>

            <!-- Modules -->
            <script src="/js/StartupHubApp.js"></script>

            <!-- Controllers -->
            <script src="/js/Controllers/HeaderController.js"></script>
            <script src="/js/Controllers/LoginController.js"></script>

            <!-- Directives -->
            <script src="/js/Directives/header.js"></script>
            <script src="/js/Directives/loginBar.js"></script>
    </body>
</html>