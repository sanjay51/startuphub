<?php
include '../db/db_connect.php';

ensure_logged_in();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER['DOCUMENT_ROOT'] . '/js/common.js';
        ?>

        <title>Create new post</title>
    </head>

    <body ng-app="StartupHubApp">
        <div ng-controller='HeaderController'>
            <app-header></app-header>
            <login-bar></login-bar>
        </div>

        <div class="container" ng-controller="PostCreationController">
            <div class="page-header">
                <h1>Create a post</h1>
            </div>
            <form class="form-horizontal" name="postForm" method="post" novalidate>
                <div class="row">
                    <label class="control-label col-sm-5">Short description </label>
                    <div class="col-sm-5">
                        <textarea required="" ng-model="post.description" name="description" type="text" class="form-control" ></textarea>
                        <div ng-show="postForm.description.$error.required" class="error-message">
                            Please enter short description.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Details </label>
                    <div class="col-sm-5">
                        <textarea required="" name="details" ng-model="post.details" type="text" class="form-control" rows="8" ></textarea>
                        <div ng-show="postForm.details.$error.required" class="error-message">
                            Please add some details.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <p align="center">
                        <input ng-show="isCreatingPost == false" class="btn btn-large btn-primary" ng-click="createPost(post)" type="submit" name="submit" value="Create post" />
                        <input class="btn btn-large btn-primary" ng-show="isCreatingPost == true" value="Please wait.." />
                    </p>
                </div>
            </form>

            <style type="text/css">
                .center_div {
                    margin: 0 auto;
                    width: 100%;
                }
                .error-message {
                    font-size: 10px;
                    color: red;
                }
            </style>

            <!-- Modules -->
            <script src="/js/StartupHubApp.js"></script>

            <!-- Controllers -->
            <script src="/js/Controllers/HeaderController.js"></script>
            <script src="/js/Controllers/PostCreationController.js"></script>

            <!-- Directives -->
            <script src="/js/Directives/header.js"></script>
            <script src="/js/Directives/loginBar.js"></script>
            
            <div id="divLog">Logs here</div>
    </body>
</html>