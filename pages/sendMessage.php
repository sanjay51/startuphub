<?php
include '../db/db_connect.php';

ensure_logged_in();
$toUserID = $_GET['to'];
$toUserName = $_GET['name'];
$relatedPostID = $_GET['post'];
$comma = "', '";

$thread_id = $_GET['tid'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER['DOCUMENT_ROOT'] . '/js/common.js';
        ?>

        <title>Send a message to <?php echo $toUserName; ?></title>
    </head>

    <body ng-app="StartupHubApp">
        <div ng-controller='HeaderController'>
            <app-header></app-header>
            <login-bar></login-bar>
        </div>

        <div class="container" ng-controller="MessageController" ng-init="thread_id='<?php echo $thread_id; ?>';isSubjectDisabled=false;threadInit('<?php echo $thread_id.$comma.$toUserID.$comma.$relatedPostID; ?>');to_id='<?php echo $toUserID; ?>';to_name='<?php echo $toUserName; ?>';related_post_id='<?php echo $relatedPostID; ?>';">
            <div class="page-header">
                <h1>Message <small>to {{ to_name }}</small></h1>
            </div>
            <form class="form-horizontal" name="messageForm" method="post" novalidate>
                <input type="hidden" ng-init="message.thread_id=thread_id" ng-model="message.thread_id" class="form-control" />
                <input type="hidden" ng-init="message.to_user_id=to_id" ng-model="message.to_user_id" class="form-control" />
                <input type="hidden" ng-init="message.related_post_id=related_post_id" ng-model="message.related_post_id" class="form-control" />
                <div class="row">
                    <label class="control-label col-sm-5">Subject </label>
                    <div class="col-sm-4">
                        <input required="" ng-disabled="isSubjectDisabled" ng-model="message.subject" name="subject" type="text" class="form-control" />
                        <div ng-show="messageForm.subject.$error.required" class="error-message">
                            Please enter a subject line.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <label class="control-label col-sm-5">Message </label>
                    <div class="col-sm-4">
                        <textarea name="message_text" ng-model="message.message_text" type="text" class="form-control" rows="8" ></textarea>
                        <div ng-show="messageForm.message_text.$error.required" class="error-message">
                            Message cannot be left blank.
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <p align="center">
                        <input ng-show="isSendingMessage == false" class="btn btn-large btn-primary" ng-click="sendMessage(message)" type="submit" name="submit" value="Send message" />
                        <input class="btn btn-large btn-primary" ng-show="isSendingMessage == true" value="Please wait.." />
                    </p>
                </div>
            </form>
            
            <div ng-repeat="message in messages">
                <div style="background-color:lightblue; box-shadow:0px 0px 20px #f79696; margin:5px; padding:10px">
                    <img width="15" height="15" ng-src="{{message.sender_image_link}}" /><b><label style="font-size:12px"> Sanjay Verma</b></label><br/>
                    &nbsp;&nbsp;<label>{{message.body}}</label><br/>
                    &nbsp;&nbsp;<label style="color:gray; font-size:10px;">{{ message.timestamp }}</label><br/>
                </div>
            </div>

            <style type="text/css">
                .center_div {
                    margin: 0 auto;
                    width: 100%;
                }
                .error-message {
                    font-size: 10px;
                    color: red;
                }
            </style>

            <!-- Modules -->
            <script src="/js/StartupHubApp.js"></script>

            <!-- Controllers -->
            <script src="/js/Controllers/HeaderController.js"></script>
            <script src="/js/Controllers/MessageController.js"></script>

            <!-- Directives -->
            <script src="/js/Directives/header.js"></script>
            <script src="/js/Directives/loginBar.js"></script>
            
            <div id="divLog">Logs here</div>
    </body>
</html>