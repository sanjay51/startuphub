<?php
include '../db/db_connect.php';

ensure_company_logged_in();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        include $_SERVER['DOCUMENT_ROOT'] . '/js/common.js';
        ?>

    </head>

    <body ng-app="StartupHubApp">
        <div ng-controller='HeaderController'>
            <app-header></app-header>
            <login-bar></login-bar>
        </div>

        <div class="container" ng-controller="CompanyProfileValidationController">
            <div class="page-header">
                <h1>Company Profile <small>About the company</small></h1>
            </div>
            <form name="form" role="form" class="form-horizontal" novalidate>
                <div class="form-group">
                    <div class="row">
                        <label for="oneliner" class="control-label col-sm-2">One liner</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" type="text" name="oneliner" required="" ng-model="company.oneliner" placeholder="One liner description of the company.." ></textarea>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="contact" class="control-label col-sm-2">Contact(*)</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control col-sm-6" required="" name="contact" ng-model="company.contact" placeholder="Contact number" ng-minlength="8" />
                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.contact.$error.minlength">Contact number should be of atleast 8 digits.</span>
                            <span ng-show="form.contact.$error.required">Contact number cannot be left blank.</span>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="website" class="control-label col-sm-2">Website</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" ng-model="company.website" name="website" placeholder="Company website here.." />
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="country" class="control-label col-sm-2">Country</label>
                        <div class="col-sm-6">
                            <select class="form-control" ng-model="company.country" name="country">
                                <option>Canada</option>
                                <option>China</option>
                                <option>India</option>
                                <option>Japan</option>
                                <option>Other</option>
                                <option>UK</option>
                                <option>USA</option>
                            </select>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="domain" class="control-label col-sm-2">Working domain</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" ng-model="company.domain" name="domain" required="" placeholder="Company's working domain, e.g. Web development" />
                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.domain.$error.required">Domain cannot be left blank.</span>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <label for="about" class="control-label col-sm-2">About </label>
                        <div class="col-sm-6">
                            <textarea class="form-control" ng-model="company.about" name="about" rows="5" placeholder="About the company" ></textarea>                                                        



                        </div>
                        <div class="col-sm-4">
                            <span ng-show="form.ustream.$error.required">About field cannot be left blank.</span>
                        </div>
                    </div>
                    <br/>

                    <div class="row">
                        <p align="center">
                            <input ng-show="isProcessingUpdation == false" class="btn btn-large btn-primary" ng-click="update(company)" type="submit" name="Submit" value="Save profile" />
                            <input class="btn btn-large btn-primary" ng-show="isProcessingUpdation == true" value="Please wait.." />
                            <a href="/">Go back</a>
                        </p>
                    </div>
                </div>
            </form>
        </div>

        <style type="text/css">
            .form-horizontal input.ng-invalid.ng-dirty {
                border-color: #FA787E;
            }

            .form-horizontal input.ng-valid.ng-dirty {
                border-color: #78FA89;
            }

            .light-heading h2 {
                font-size: 20px;
                opacity: 0.5;
            }
        </style>

        <!-- Modules -->
        <script src="/js/StartupHubApp.js"></script>

        <!-- Controllers -->
        <script src="/js/Controllers/HeaderController.js"></script>
        <script src="/js/Controllers/CompanyProfileValidatorController.js"></script>

        <!-- Directives -->
        <script src="/js/Directives/header.js"></script>
        <script src="/js/Directives/loginBar.js"></script>
    </body>
</html>